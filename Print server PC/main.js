const { app, BrowserWindow, net } = require('electron');
const fs = require('fs');
const request = require('request');
let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({ width: 100, height: 100, x: 1100, y: 650 }); //TODO change?
    printImages();
    setInterval(printImages, 10000);
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

function printImages() {
    downloadJSONs().then(data => {
        if (data.length > 0) {
            data.forEach(element => {
                if (element.imageUrl != null && element.imageUrl != undefined) {
                    console.log('printing...');
                    let child = new BrowserWindow({ width: 2400, height: 1800, parent: mainWindow, modal: true, show: false });
                    child.loadURL(element.imageUrl);
                    setTimeout(function () { });
                    child.webContents.on('did-finish-load', () => {
                        child.show();
                        fs.readFile(__dirname + '/print.css', 'utf-8', function (error, data) {
                            if (!error) {
                                child.webContents.insertCSS(data);
                                setTimeout(function () {
                                    child.webContents.print({ silent: true }, () => {
                                        child.close();
                                    });
                                }, 3000);
                                clearJSON(element.code);
                            }
                        });
                    });
                } else {
                    console.log('deleting print....');
                    clearJSON(element.code);
                }
            });
        } else {
            console.log('nothing to print');
        }
    });
}

function downloadJSONs() {
    const request = net.request({
        method: 'GET',
        protocol: 'http:',
        hostname: '192.168.1.3/retrieveQueue.php' //TODO change?
    });
    return new Promise(function (resolve, reject) {
        request.on('response', response => {
            response.on('data', chunk => {
                resolve(eval(chunk.toString()));
            });
        });
        request.end();
    });
}

function clearJSON(cd) {
    const options = {
        method: 'POST',
        url: 'http://192.168.1.3/clearPrintQueue.php', //TODO change?
        headers: {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: { code: cd }
    };
    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        console.log(body);
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow();
    }
});
