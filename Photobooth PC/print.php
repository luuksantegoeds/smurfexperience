<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Print een DYMO label</title>
<script src = "DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"> </script>
<script src = "dymo.js" type="text/javascript" charset="UTF-8"> </script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.8.3.js"></script>
</head>
 
<body>
<h2>Print een DYMO label</h2>
 
<div id="printDiv">
    <button id="printKnop">Print LABEL</button>
</div>

<?php

$handle = fopen("template.xml", "r") or die("Couldn't get handle");
$content = "";
if ($handle) {
    while (!feof($handle)) {
        $buffer = fgets($handle, 4096);
        $content .= $buffer;
    }
    fclose($handle);
}

echo "<textarea id='xml' hidden>$content</textarea>";

?>
 
</body>
</html>