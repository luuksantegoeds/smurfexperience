<?php
  require_once("lib/helper.php");

  header("Access-Control-Allow-Origin: *");

  //Check for code
  if(!isset($_GET["code"])) {
    print buildOutput(false, null, "missing GET parameter");
    exit;
  }

  //Set base filename
  $baseFileName = $_GET["code"];
  $fileName = null;

  //First check if we already have the file available
  $localStream = opendir(getcwd()."/original/");
  while(($file = readdir($localStream)) !== false) {
    if($file === "." || $file === "..")
      continue;

    //Filename parts
    $nameParts = explode(".", $file);
    if(count($nameParts) != 2)
      continue;

    //Find code
    if($baseFileName === $nameParts[0])
      $fileName = $file;
  }
  closedir($localStream);

  //If fileName is not null, we have it - return and stop
  if($fileName) {
    print buildOutput(true, "http://192.168.1.3/original/$fileName");
    exit;
  }

  //Apparently we don't have it, so try to retrieve if from FTP
  $connection = ssh2_connect("192.168.1.3", 22);
  if(!ssh2_auth_password($connection, "pi", "bebotics")) {
    print buildOutput(false, null, "sftp connection authentication failed");
    exit;
  }

  //Get instance
  $sftp = ssh2_sftp($connection);
  $filePath = "ssh2.sftp://".intval($sftp)."/home/pi/files/";
  $fileName = null;

  //Find image file
  $remoteStream = opendir($filePath);
  while(($file = readdir($remoteStream)) !== false) {
    if($file === "." || $file === "..")
      continue;

    //Filename parts
    $nameParts = explode(".", $file);
    if(count($nameParts) != 2)
      continue;

    //Find code
    if($baseFileName === $nameParts[0])
      $fileName = $file;
  }
  closedir($remoteStream);

  //Output
  if($fileName) {
    $targetPath = getcwd()."/original/$fileName";
    $remotePath = "ssh2.sftp://".intval($sftp)."/home/pi/files/$fileName";
    if(file_put_contents($targetPath, file_get_contents($remotePath, $targetPath)))
      print buildOutput(true, "http://192.168.1.3/original/$fileName");
    else
      print buildOutput(false, null, "failed to download file");
  }
  else
    print buildOutput(false, null, "file could not be found");

?>
