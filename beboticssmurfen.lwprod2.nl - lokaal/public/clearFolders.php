<?php

require_once "lib/helper.php";

$files = glob('/var/www/html/original/*');
foreach ($files as $file) {
    if (is_file($file)) {
        unlink($file);
    }
}

$files = glob('/var/www/html/modified/*');
if(count($files) > 50){
    $originalSize = count($files);
    for($i = count($files); $i > ($originalSize - 51); $i--){
        //Save 50 photos for the LiveWall
        unset($files[$i]);
    }

    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        }
    }
}

print "success";