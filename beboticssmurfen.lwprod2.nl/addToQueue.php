<?php

require_once "lib/helper.php";

//Check for code
if (!isset($_POST["code"]) || !isset($_POST["imageUrl"])) {
    print buildOutput(false, null, "missing POST parameter");
    exit;
}

//Try to load JSON
$jsonPath = getcwd() . "/acceptQueue.json";
$jsonData = file_get_contents($jsonPath);
$queueData = array();

if ($jsonData) {
    try {
        $queueData = json_decode($jsonData, true);
    } catch (Exception $e) {}
}

$alreadyInQueue = false;
foreach ($queueData as $item) {
    if ($item['code'] == $_POST['code']) {
        $alreadyInQueue = true;
        break;
    }
}

if (!$alreadyInQueue) {
    //Add data to queue
    $queueData[] = array(
        "code" => $_POST['code'],
        "imageUrl" => $_POST['imageUrl'],
    );

    //Store data again
    if (file_put_contents($jsonPath, json_encode($queueData))) {
        include("increaseRequested.php");
        print buildOutput(true, null, "queue data updated");
    } else {
        print buildOutput(false, null, "failed to update queue data");
    }
} else {
    print buildOutput(false, null, "Photo already in queue");
}
