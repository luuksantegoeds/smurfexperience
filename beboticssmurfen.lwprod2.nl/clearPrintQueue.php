<?php

if (!isset($_POST['code'])) {
    print "error, no code given";
    die();
}

//Load acceptQueue
$jsonPath = getcwd() . "/printQueue.json";
$output = null;

//Check if file exists & is parcable
if (file_exists($jsonPath)) {
    $jsonAccept = file_get_contents($jsonPath);

    try {
        $output = json_decode($jsonAccept, true);
        if (!$output) {
            $output = array();
        }

    } catch (Exception $e) {}
} else {
    $output = array();
}

$keyToRemove = -1;

for ($i = 0; $i < count($output); $i++) {
    if ($output[$i]['code'] == $_POST['code']) {
        $keyToRemove = $i;
        break;
    }
}

if ($keyToRemove >= 0) {
    unset($output[$keyToRemove]);
}

file_put_contents($jsonPath, json_encode(array_values($output)));